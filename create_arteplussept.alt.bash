#!/bin/bash
# addon.py (present at plugin.video.arteplussept/resources/lib/addon.py
# plugin.video.arteplussept/addon.xml
# plugin.video.arteplussept/CHANGELOG.md
# plugin.video.arteplussept/changelog.txt
# plugin.video.arteplussept/.gitignore
# plugin.video.arteplussept/resources/language/English/strings.xml
# plugin.video.arteplussept/resources/language/French/strings.xml
# plugin.video.arteplussept/resources/language/German/strings.xml
# plugin.video.arteplussept/resources/settings.xml


echo "Script to use under Linux."
echo "To use, please install git."
read -n 1 -s -r -p "Press a key to continue (or CTRL-C to cancel)..."

git clone https://github.com/known-as-bmf/plugin.video.arteplussept
mv plugin.video.arteplussept plugin.video.arteplussept.alt
cd plugin.video.arteplussept.alt

# sed -i -e '/pattern/r filetoinsert' filetobeinserted
echo "Add code to resources/settings.xml..."
read -n 1 -s -r -p "Press a key to continue (or CTRL-C to cancel)..."
cd resources
sed -i -e '/30052/r ../../settings.xml.add' settings.xml

#echo "Create zip file of plugin and remove working folder."
#read -n 1 -s -r -p "Press a key to continue (or CTRL-C to cancel)..."
#zip -r plugin.video.arteplussept.alt plugin.video.arteplussept.alt/*
#rm -f -r plugin.video.arteplussept.alt

